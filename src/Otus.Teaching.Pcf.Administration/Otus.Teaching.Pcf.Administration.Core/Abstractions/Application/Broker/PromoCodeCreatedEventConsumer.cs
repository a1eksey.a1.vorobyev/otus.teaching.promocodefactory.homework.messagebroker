﻿using MassTransit;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Abstractions.Application.Broker
{
    public class PromoCodeCreatedEventConsumer : IConsumer<PromoCodeCreatedEvent>
    {
        private readonly IEmployeeService _employeeService;

        public PromoCodeCreatedEventConsumer(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        public async Task Consume(ConsumeContext<PromoCodeCreatedEvent> context)
        {
            if (context.Message.PartnerManagerId != null)
                await _employeeService.UpdateAppliedPromocodesAsync(context.Message.PartnerManagerId.Value);
        }
    }
}